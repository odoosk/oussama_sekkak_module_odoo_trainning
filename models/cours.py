# -*- coding: utf-8 -*-
###################################################################################
#
#    Cybrosys Technologies Pvt. Ltd.
#    Copyright (C) 2018-TODAY Cybrosys Technologies(<http://www.cybrosys.com>).
#    Author: cybrosys(<https://www.cybrosys.com>)
#
#    This program is free software: you can modify
#    it under the terms of the GNU Affero General Public License (AGPL) as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###################################################################################
import pytz
import sys
import datetime
import logging
import binascii

from struct import unpack
from odoo import api, fields, models
from odoo import _
from odoo.exceptions import UserError, ValidationError

_logger = logging.getLogger(__name__)


class GsaCours(models.Model):
	_name = 'gsa.cours'
	_order = 'name desc'
	_description = "Cours"


	#declaration of fields
	
	name= fields.Char('Title of module',required=True)
	description = fields.Text('Description')
	theme_id = fields.Many2one('gsa.theme','Theme')
	duration = fields.Integer('Duration',help='Number of hours scheduled for the course')
	
	#this is to add the tittle of theme next to the tittle of cours 
	#so for that we need to override the famous funtion name_get
	def name_get(self):
		res = []
		for cours in self:
			name = cours.name
			if cours.theme_id.name:
				name = '%s - %s' % (name, cours.theme_id.name)
			res.append((cours.id, name))
		return res
		return super(GsaCours, self).name_get()

class GsaTheme(models.Model):
	_name = 'gsa.theme'
	_order = 'name desc'
	_description = "Theme"



	name = fields.Char('Theme')