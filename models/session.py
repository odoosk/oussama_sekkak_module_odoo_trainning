# -*- coding: utf-8 -*-
###################################################################################
#
#    Cybrosys Technologies Pvt. Ltd.
#    Copyright (C) 2018-TODAY Cybrosys Technologies(<http://www.cybrosys.com>).
#    Author: cybrosys(<https://www.cybrosys.com>)
#
#    This program is free software: you can modify
#    it under the terms of the GNU Affero General Public License (AGPL) as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###################################################################################
import pytz
import sys
from datetime import datetime ,timedelta
import logging
import binascii

from struct import unpack
from odoo import api, fields, models
from odoo import _
from odoo.exceptions import UserError, ValidationError

_logger = logging.getLogger(__name__)


class GsaSession(models.Model):
	_name = 'gsa.session'
	_session = 'name desc'
	_description = "Session"
	_inherit = ['mail.thread']
	_order = 'start_date asc'


	#calculate number of days depends of start date and end date , the result is integer , 
	@api.depends('start_date','end_date')
	def _compute_duration_days(self):
		#since odoo 13 , this loop replace all of api.multi , api.model , api.one
		for record in self:
			duration = 0
			if record.start_date and record.end_date:
				duration = (record.end_date - record.start_date).days
			else:
				duration = 0
			record.duration = duration



	@api.depends('start_date','end_date')
	def _compute_duration_hours(self):
		for record in self:
			duration = 0
			if record.start_date and record.end_date :
				#convert hours to string and cast the result to integer ( its for calculate delta hours)
				start_hours = datetime.strftime(record.start_date,"%H") # convert string to time
				end_hours = datetime.strftime(record.end_date,"%H")

				#convert hours to string and cast the result to integer ( its for calculate delta hours)
				star_day = datetime.strftime(record.start_date,"%d")
				end_day  = datetime.strftime(record.end_date,"%d")

				#convert hours to string and cast the result to integer ( its for calculate delta hours)
				start_week = datetime.strftime(record.start_date,"%V") 
				end_week = datetime.strftime(record.end_date,"%V")

				#after the preparation of the values of the attributes of the function timedelta of datetime, 
				#we apply to the result the second function total_seconds 
				#to have at the end the number of hours in division over 3600
				duration = timedelta(weeks=int(end_week)-int(start_week) ,days=int(end_day)- int(star_day), hours=int(end_hours) - int(start_hours)).total_seconds() / 3600
				
				record.duration_hours = duration
			else:
				record.duration_hours = 0
			

	#declaration of fields
	lesson_id = fields.Many2one('gsa.cours','Lesson',required=True,tracking=5, index=True)
	start_date = fields.Datetime('Start Date')
	end_date = fields.Datetime('End Date')
	duration = fields.Integer(string='Duration Days',compute='_compute_duration_days')
	duration_hours = fields.Float(string="Duration Hours", compute='_compute_duration_hours')
	trainer_id = fields.Many2one('res.partner','Trainer',domain=[('is_trainer','=','True')] ,tracking=1, index=True)
	max_participant = fields.Integer('Max participant',required=True)
	participant_ids = fields.Many2many('res.partner','res_partner_session_rel','partner_id','session_id','Participants',copy=False)
	state = fields.Selection([('draft','Draft'),('scheduled','Scheduled'),('open','Open'),('close','Ended')],default='draft')



	#this is to add the tittle of the session next to the tittle of lesson 
	#so for that we need to override the famous funtion name_get
	def name_get(self):
		res = []
		for session in self:
			name = ''
			#check if lesson , and start date have been entered
			if session.lesson_id.name and session.start_date :
				name = '%s - %s' % (session.start_date, session.lesson_id.name)
			res.append((session.id, name))
		return res
		return super(GsaSession, self).name_get()


	#the start date must be lower than the end date, that's logical no ! ^_^
	@api.onchange('start_date', 'end_date')
	def _check_dates(self):
		for rec in self:
			if rec.start_date and rec.end_date and rec.start_date > rec.end_date:
				raise ValidationError((_("Attention, start date is greater than the end date\n   (%s) > (%s).") % (rec.start_date, rec.end_date)))
	
			if rec.end_date and rec.end_date < datetime.now():
				raise ValidationError((_("Please enter a date greater than now  ^_^ \n")))
					

	#the trainer can not be participant in the same session but he can be participant in other session that why we used boolean field
	#instead of selection and we did not add a domain in participant_ids fields
	#so this function resolve all
	@api.constrains('participant_ids', 'trainer_id','max_participant')
	def _check_trainer(self):
		for rec in self:
			for participant in rec.participant_ids:
				#in this method odoo creates a new object so to get the current id record in database we use ._origin.id
				if rec.trainer_id.id == participant._origin.id:
					raise ValidationError((_("(%s) can not be a participant and trainer at the same time") % (rec.trainer_id.name))) 
			#check if we can subscribe more of of participants
			if len(rec.participant_ids) > rec.max_participant:
				raise ValidationError((_("The maximum number of participants (%s) is exceeded") % (rec.max_participant))) 
						
					

	def do_open(self):
		for rec in self:
			rec.state = 'open'


	def do_open(self):
		for rec in self:
			rec.state = 'open'

	def do_close(self):
		for rec in self:
			rec.state = 'close'

	def do_cancel(self):
		for rec in self:
			rec.state = 'draft'

	def do_scheduled(self):
		for rec in self:
			rec.state = 'scheduled'