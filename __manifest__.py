# -*- coding: utf-8 -*-
##########################################################################
###################################################################################
{
    'name': 'Gestion des sessions d\'apprentissage',
    'version': '13.0.1.1.2',
    'summary': """Sessions, Cours, Formateur, Participants""",
    'description': """Ce module vous permet de gérer les sessions d'apprentissage""",
    'category': 'all',
    'author': 'SEKKAK OUSSAMA',
    'company': 'IT4LIFE',
    'website': "",
    'depends': ['base', 'contacts','mail'],
    'data': [
        'security/gsa_security.xml',
        'security/ir.model.access.csv',
        'views/session_view.xml',
        'views/cours_view.xml',
        'views/partner_view.xml',
        'views/menus.xml',
    ],
    'icon_image': ['static/description/icon.png'],
    'images': ['static/description/access_group.png','static/description/session.png','static/description/session_trainer.png'],
    'license': 'AGPL-3',
    'demo': [],
    'installable': True,
    'auto_install': False,
    'application': False,
}
